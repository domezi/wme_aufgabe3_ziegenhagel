const express = require('express');
const app = express();
const sys = require('util');
const path = require('path');
const bodyParser = require('body-parser');
const csv= require('csvtojson');
const {param} = require("express/lib/router");
const ObjectsToCsv = require("objects-to-csv");



app.use( bodyParser.json() );
app.use( express.static( path.join(__dirname, "svelte-app/public") ) ); //register public dir to serve static files (html, css, js)

// csv to json
let loadData = new Promise(function(resolve, reject) {
	csv()
		.fromFile("world_data.csv")
		.then((data)=>{
			// const data_json=JSON.stringify(data) // to json string as requested in a1
			resolve(data)
		})
})
let persistData = (data) => {
	return new Promise(async function (resolve, reject) {

		const ObjectsToCsv = require('objects-to-csv')
		const csv = new ObjectsToCsv(data)
		await csv.toDisk('world_data.csv')

	})
}

// routes
app.get('/items', function (req, res) {
	loadData.then((data)=>{
		res.send(data)
	})
})
app.get('/items/:id([0-9]+)', function (req, res) {
	loadData.then((data)=>{
		let items_found = data.filter(item=>parseInt(item.id) === parseInt(req.params.id))
		if(items_found.length>0)
			res.send(items_found[0])
		else
			res.send({status:`No such id ${req.params.id} in "database".`, success: false})
	})
})
app.get('/items/:first([0-9]+)/:last([0-9]+)', function (req, res) {
	loadData.then((data)=>{
		let items_found = data.filter(item=>parseInt(item.id) >= parseInt(req.params.first) && parseInt(item.id) <= parseInt(req.params.last))
		if(items_found.length>0)
			res.send(items_found)
		else
			res.send({status:`Range not possible.`, success: false})
	})
})
app.get('/properties', function (req, res) {
	loadData.then((data)=>{
		res.send(Object.keys(data[0]))
	})
})
app.get('/properties/:num([0-9]+)', function (req, res) {
	loadData.then((data)=>{
		let property = Object.keys(data[0])[parseInt(req.params.num)]
		res.send(property || {status:"No such property available.", success: false})
	})
})
app.post('/items', function (req, res) {
	loadData.then((data) => {
		let properties = Object.keys(data[0])

		// validation
		let validated = Object.keys(req.body).filter(key => properties.includes(key) && key !== "name")

		if (req.body === undefined)
			return res.send({status: "No params found.", success: false})
		else if (req.body.name === undefined || req.body.name == "")
			return res.send({status: "Missing param 'name'.", success: false})
		else if (validated.length < 2)
			return res.send({status: "Not enough valid params found, at least two required.", success: false})

		// create item
        let id = parseInt(data[data.length-1].id)+1
		let item = {...req.body, id}
		data.push(item)
		persistData(data).then(d=>console.log("added",item))

		// send created item copy
		res.send({status: "Added country " + req.body.name + " to list!", success: true})
	})
})
app.delete('/items', function (req, res) {
	loadData.then((data) => {
		let deleted_item = data.pop()
		persistData(data).then(d=>console.log("deleted",item))
		res.send({status: "Deleted last country: " + deleted_item.name + "!", success: true})
	})
})
app.delete('/items/:id([0-9]+)', function (req, res) {
	loadData.then((data) => {

		let index = -1
		let items_found = data.filter((item, key) => parseInt(item.id) === parseInt(req.params.id) && (index = key))
		if(items_found.length === 0)
			return res.send({status: `No such id ${req.params.id} in "database"`, success: false})

		data.splice(index, 1);

		persistData(data).then(d=>console.log("deleted",item))
		res.send({status: `Item ${req.params.id} deleted successfully.`, success: true})
	})
})


app.listen(3000,"0.0.0.0", () => { console.log(`Example app listening`); });
